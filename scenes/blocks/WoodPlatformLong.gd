extends StaticBody2D

func _on_HitDetection_body_entered(body):
	if body.is_in_group("cannonball"):
		#If we've been hit by a cannonball, wobble
		$HitSound.play()
