extends RigidBody2D

var track_tank : bool = false
var velocity = Vector2()
var direction_to_tank = Vector2()

const explosion_scaler : float = 4.75
const shockwave_scaler : float = 18.0

export (bool) var exploded = false
var detonated : bool  = false
export (int) var speed = 240

var explosion_animation
var explosion 

func _ready():
	add_to_group("block")
	randomize()
	explosion_animation = load("res://scenes/ExplosionAnimation.tscn")
		
func _physics_process(delta):
	if track_tank and !exploded:
		apply_impulse(Vector2(), direction_to_tank.normalized() * speed)
		

func explode():
	exploded = true
	$CountdownAnimation.visible = false
	mass = 5
	friction = 1
	$ExplosionSound.play()
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a cannonball explosion and attach it to the parent level 
		explosion = explosion_animation.instance()
		get_parent().add_child(explosion) #Add to level node
		explosion.position = self.global_position
		$Sprite.visible = false #Hide the cannonball picture
		#print("Spawn explosion animation")
		$ExplosionTween.interpolate_property(explosion, "scale", explosion.scale , Vector2(explosion_scaler, explosion_scaler), 0.6, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		
	$ExplosionTween.interpolate_property($ExplosionShockwave, "scale", $ExplosionShockwave.scale , Vector2(shockwave_scaler, shockwave_scaler), 0.6, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$ExplosionTween.interpolate_property(self, "speed", self.speed, 1, 0.6, Tween.TRANS_LINEAR , Tween.EASE_OUT)	
	$ExplosionTween.interpolate_property(self, "mass", self.mass, 0.1, 0.6, Tween.TRANS_LINEAR , Tween.EASE_OUT)
	$ExplosionTween.start()
	Utils.shake(3, 0.5, 0)

func _on_ExplosionTween_tween_completed(object, key):
	#queue_free()
	pass


func _on_HitArea_body_entered(body):
	if body.is_in_group("cannonball"):
		$HitSound.stream = load("res://assets/sounds/metal_" + str(randi() % 6 +1) + ".wav")
		#If we've been hit by a cannonball, wobble
		$HitSound.play()
		$HitTween.interpolate_property($Sprite, "scale", $Sprite.scale , Vector2(0.85, 0.85), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		$HitTween.start()
		$HitTween.interpolate_property($Sprite, "scale", $Sprite.scale , Vector2(1.0, 1.0), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		$HitTween.start()
		
	if body.is_in_group("tank"):
		if $DetonationSound.stream_paused:
			$DetonationSound.stream_paused = false
		else:
			$DetonationSound.play()
			
		$DetonationTimer.start()
		$CountdownAnimation.visible = true
		$CountdownAnimation.play()
		detonated = true
		#We stop the mine moving
		direction_to_tank = Vector2.ZERO
		
		

func _on_TrackArea_body_entered(body):
	if body.is_in_group("tank"):
		#The tank has entered our area of magnetic attraction. Pursue tank while its within this area
		#print("track tank direction:" + str(direction_to_tank))
		if track_tank:
			#If we're already in tank track mode, push forward the tracking stop timer
			$TrackingTimer.stop()
		else:
			track_tank = true
		direction_to_tank = body.global_position - global_position
		$TrackingSound.play()


func _on_TrackArea_body_exited(body):
	if body.is_in_group("tank"):
		#The tanke has exited our area of magnetic attraction. 
		#chase tank for a spell, then give up
		direction_to_tank = body.global_position - global_position
		$TrackingTimer.start()
		#If the tank has left our tracking area, call off detonation.
		track_tank = true
		if detonated:
			#if the detonation timer was going, then we stop the detonation process
			$DetonationSound.stream_paused = true
			$DetonationTimer.stop()
			$CountdownAnimation.visible = false
			$CountdownAnimation.stop()
			print("Stop orange detonation")
			detonated = false


func _on_TrackingTimer_timeout():
	#print("stop tracking tank")
	track_tank = false


func _on_ExplosionSound_finished():
	#Explosion finished. Free node.
	queue_free()

func _on_DetonationSound_finished():
	#Detonation countdown finished. Explode.
	explode()


func _on_DetonationTimer_timeout():
	$HitTween.interpolate_property($Sprite, "scale", $Sprite.scale , Vector2(0.85, 0.85), 0.45, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$HitTween.start()
	$HitTween.interpolate_property($Sprite, "scale", $Sprite.scale , Vector2(1.0, 1.0), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$HitTween.start()
	detonated = true
