extends RigidBody2D

export (int) var speed = 600
export (bool) var exploded = false

var explosion_animation
var explosion 

func _ready():
	add_to_group("cannonball")
	explosion_animation = load("res://scenes/ExplosionAnimation.tscn")
	

func _on_ExplosionTimer_timeout():
	self.mass = 100
	exploded = true
	mass = 10
	friction = 1
	$ExplosionSound.play()
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a cannonball explosion and attach it to the parent level 
		explosion = explosion_animation.instance()
		get_parent().add_child(explosion) #Add to level node
		explosion.position = self.global_position
		$Sprite.visible = false #Hide the cannonball picture
		#print("Spawn explosion animation")
		$ExplosionTween.interpolate_property(explosion, "scale", explosion.scale , Vector2(3.0, 3.0), 0.6, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		
	$ExplosionTween.interpolate_property($ExplosionShockwave, "scale", $ExplosionShockwave.scale , Vector2(12.0, 12.0), 0.6, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	#print("speed before: " + str(self.speed))
	$ExplosionTween.interpolate_property(self, "speed", self.speed, 1, 0.6, Tween.TRANS_LINEAR , Tween.EASE_OUT)
	#print("mass before: " + str(self.mass))
	$ExplosionTween.interpolate_property(self, "mass", self.mass, 0.1, 0.6, Tween.TRANS_LINEAR , Tween.EASE_OUT)
	#print ("exploded: " + str(exploded))
	$ExplosionTween.start()
	Utils.shake(10, 0.5, 0)
	self.sleeping = true
	
func _on_ExplosionTween_tween_completed(object, key):
	#explosion.queue_free()
	queue_free()
