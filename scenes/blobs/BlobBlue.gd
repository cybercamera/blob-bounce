extends RigidBody2D

var SpawnBlob
var speed : int = 400
const hit_points = 100
var hit_direction = Vector2()

export var can_split: bool = false
export var generation_counter: int = 3 #After x generations of spawning, this node will die when hit by explosion

var hit: bool = false

func _ready():
	add_to_group("blob")
	SpawnBlob = load("res://scenes/blobs/BlobBlueSmall.tscn")
	Utils.increment_total_blobs()

func wobble():
	#print("Wobble direction: " + str(direction))
	$WobbleTween.interpolate_property(self, "scale", scale , Vector2(0.75, 0.75), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	#$WobbleTimer.start()
	
func spawn():
	#blob has been hit by explosion. Split in two
	#print("blobbluesmall spawn")
	scale = Vector2(0.5, 0.5)
	var new_blob = SpawnBlob.instance()
	get_tree().get_root().add_child(new_blob)
	#print("get_parent()" + str(get_parent().name))
	new_blob.generation_counter = generation_counter -1 #set the new blob to be the same level as the current generation counter
	$WobbleTween.interpolate_property(new_blob, "scale", scale , Vector2(0.5, 0.5), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.interpolate_property(new_blob, "position", position , self.position, 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	can_split = false
	$NewCloneTimer.start()
	
func die():
	Utils.increment_score(hit_points)
	Utils.deccrement_total_blobs()
	queue_free()

func _on_Area2D_body_entered(body):
	#print("_on_Area2D_body_entered(" + str(body) + ")")
	hit_direction = self.position - body.position
	if body.get_groups().has("cannonball"):
		if body.exploded == true and can_split:
			#generate a random number of small greens
			var spawn_num = randi()%6+2
			print(str(spawn_num))
			for i in range (0, spawn_num):
				spawn()
				
			$DeathTimer.start()
			self.visible = false
			
		wobble()
		$AudioStreamPlayer.play()
		

func _on_WobbleTimer_timeout():
	print("_on_WobbleTimer_timeout")
	$WobbleTween.interpolate_property(self, "scale", Vector2(0.75, 0.75) , Vector2(1.0, 1.0), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	

func _on_NewCloneTimer_timeout():
	#In case we were a new clone, it's now time to not be new any more
	can_split = true


func _on_ImpulseTimer_timeout():
	hit_direction = Vector2(rand_range(-1,1), rand_range(-1,1))
	apply_impulse(Vector2(), hit_direction.normalized() * speed)
	#print("Green impulse: " + str(hit_direction))


func _on_DeathTimer_timeout():
	die()
