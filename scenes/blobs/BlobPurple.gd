extends RigidBody2D

var hit = false
var can_die: bool = false
const hit_points = 200
var hit_direction = Vector2()
var speed : int = 400

func _ready():
	add_to_group("blob")
	Utils.increment_total_blobs()
	#we are created sleeping
	self.sleeping = true

func die():
	Utils.increment_score(hit_points)
	Utils.deccrement_total_blobs()	
	queue_free()

func wobble(direction):
	#print("Wobble direction: " + str(direction))
	$WobbleTween.interpolate_property(self, "scale", scale , Vector2(0.75, 0.75), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	
func _on_Area2D_body_entered(body):
	#print("_on_Area2D_body_entered(" + str(body) + ")")
	var pos_to_body = Vector2()
	pos_to_body = self.position - body.position
	if body.get_groups().has("cannonball"):
		wobble(pos_to_body)
		$AudioStreamPlayer.play()
		if body.exploded == true and can_die:
			die()


func _on_CanDieTimer_timeout():
	self.sleeping = true
	can_die = true
	#This blob can now be killed.


func _on_ImpulseTimer_timeout():
	hit_direction = Vector2(rand_range(-1,1), rand_range(-1,1))
	apply_impulse(Vector2(), hit_direction.normalized() * speed)
	if position.x < 0 or position.x > Utils.screen_size.x or position.y < 0 or position.y > Utils.screen_size.y:
		#Our blob has left the screen, so player can't kill it, so we must free it for them
#		print("purple position: " + str(position))
#		print("Utils.screen_size: " + str(Utils.screen_size))
		queue_free()
