extends RigidBody2D

var SpawnBlob
var speed : int = 350
const hit_points = 100
var hit_direction = Vector2()


export var can_split: bool = false
export var generation_counter: int = 2 #After x generations of spawning, this node will die when hit by explosion

var hit: bool = false

func _ready():
	add_to_group("blob")
	SpawnBlob = load("res://scenes/blobs/BlobPurple.tscn")
	Utils.increment_total_blobs()

func wobble(direction):
	#print("Wobble direction: " + str(direction))
	$WobbleTween.interpolate_property(self, "scale", scale , Vector2(0.75, 0.75), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	#$WobbleTimer.start()
	
	
func spawn():
	#blob has been hit by explosion and is on the second generation. Thus spawn purples.
	var new_blob = SpawnBlob.instance()
	get_tree().get_root().add_child(new_blob)
	$WobbleTween.interpolate_property(new_blob, "scale", scale , Vector2(0.85, 0.85), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.interpolate_property(new_blob, "position", position , self.position, 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	can_split = false
	$NewCloneTimer.start()
	
func die():
	Utils.increment_score(hit_points)
	Utils.deccrement_total_blobs()
	queue_free()

func wait_seconds_to_spawn(sec):
	var t = Timer.new()
	t.set_wait_time(sec)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	
func _on_Area2D_body_entered(body):
	#print("_on_Area2D_body_entered(" + str(body) + ")")
	var pos_to_body = Vector2()
	pos_to_body = self.position - body.position
	if body.get_groups().has("cannonball"):
		if body.exploded == true and can_split:
			#generate a random number of purples
			for i in range (1, randi()%6+3): 
				spawn()
				wait_seconds_to_spawn(0.2)
				
			self.visible = false
			$DeathTimer.start()
			
		wobble(pos_to_body)
		$AudioStreamPlayer.play()


func _on_WobbleTimer_timeout():
	#print("_on_WobbleTimer_timeout")
	$WobbleTween.interpolate_property(self, "scale", Vector2(0.75, 0.75) , Vector2(1.0, 1.0), 0.15, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$WobbleTween.start()
	

func _on_NewCloneTimer_timeout():
	#In case we were a new clone, it's now time to not be new any more
	can_split = true
	
func _on_ImpulseTimer_timeout():
	hit_direction = Vector2(rand_range(-1,1), rand_range(-1,1))
	apply_impulse(Vector2(), hit_direction.normalized() * speed)
	#print("Gree small impulse: " + str(hit_direction))

func _on_DeathTimer_timeout():
	die()
