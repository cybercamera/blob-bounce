extends Node2D

var animation_counter : int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	#print("ExplosionAnimation_ready()")
	self.visible = true
	$Animation.visible = true
	$Animation.play()
	$Particles2D.visible = true
	$Particles2D.emitting = true
	$FadeTween.interpolate_property(self, "modulate", self.modulate, Color(self.modulate.a, self.modulate.a, self.modulate.a, 0), 1.45, Tween.TRANS_QUART, Tween.EASE_OUT)
	$FadeTween.start()
	

func _on_QueueFreeTimer_timeout():
	#print("_on_QueuFreeTimer_timeout")
	queue_free()



func _on_FadeTween_tween_all_completed():
	#print("_on_FadeTween_tween_all_completed()")
	$QueueFreeTimer.start()
