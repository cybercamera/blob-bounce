extends Node


func _ready():
	$Music.play()
	Utils.current_level = 1
	$HUD.show_title("Level " + str(Utils.current_level))
	Utils.next_level = "res://scenes/levels/Level" + str(Utils.current_level + 1) + ".tscn"

