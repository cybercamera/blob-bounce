extends TextureRect


func _ready():
	Utils.top_scores.clear()
	MakeFauxTopScores()
	#Globals.SaveScores()
	GetIntroText()
	GetCreditsText()
	Utils.screen_size = get_viewport_rect().size
	

func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		_on_Quit_pressed()
	if Input.is_action_pressed("ui_accept"):
		_on_Play_pressed()

func GetCreditsText():
	var f = File.new()
	if not f.file_exists("res://credits.txt"):
		print("No intro text file!")
		return
	
	if f.open("res://credits.txt", File.READ) != 0:
		print("Error opening file")
		return
	$Description.text = $Description.text + "\n\n\t\t\t\t\tGame Assets Credits\n\n" + f.get_as_text()
		
func GetIntroText():
	var f = File.new()
	if not f.file_exists("res://intro_text.txt"):
		print("No intro text file!")
		return
	
	if f.open("res://intro_text.txt", File.READ) != 0:
		print("Error opening file")
		return
		
	$Description.text = f.get_as_text()
	
func MakeFauxTopScores():
	if Utils.top_scores.size() > 0:
		return #Do nothing if we've already been here.
		
	var top_scores_text
	top_scores_text = Utils.load_scores()
	if !top_scores_text:
		top_scores_text = "\t\t\t\t\tHall of Fame\n\n"
		#There are no top scores, so we make some up and save them.
		Utils.top_scores[0] = ["conz", 21000]
		Utils.top_scores[1] = ["eliana", 19500]
		Utils.top_scores[2] = ["eliana", 19000]
		for i in range(Utils.top_scores.size()):
			print("score: " + str(Utils.top_scores.values()[i]))
			top_scores_text = top_scores_text + str("%04d" % int(Utils.top_scores.keys()[i])) + ":\t " +  str("%010d" % Utils.top_scores.values()[i][1]) + "\t\t" + str(Utils.top_scores.values()[i][0]) + "\n"
			print(top_scores_text)
			
	$TopScores.text = top_scores_text
	

func _on_Quit_pressed():
	print("_on_Quit_pressed()")
	get_tree().quit()


func _on_Play_pressed():
	get_tree().change_scene("res://scenes/levels/Level1.tscn")
	

func _on_PlayerName_text_entered(new_text):
	Utils.player_name = new_text
	_on_GetPlayerName_confirmed()


func _on_GetPlayerName_confirmed():
	Utils.player_name = $GetPlayerName/PlayerName.text
	#get_tree().change_scene("res://scenes/levels/Level1.tscn")


func _on_LinkButton_pressed():
	OS.shell_open($LinkButton.text)
