extends Node
#This code is orginally from: https://github.com/jotson/camera_shake_tutorial/blob/main/shake.gd

const NEW_LIFE_1 = 10000
const NEW_LIFE_2 = 15000
const NEW_LIFE_3 = 25000
const NEW_LIFE_4 = 50000
const NEW_LIFE_5 = 100000

const MAX_LIVES_LEFT = 5

const TOP_SCORES_FILE = "res://top_scores.json"

var top_scores = {}
var player_name : String = "Eliana"
var paused : bool = false

var new_life_achieved = { "NEW_LIFE_1": false,  "NEW_LIFE_2": false, "NEW_LIFE_3": false, "NEW_LIFE_4": false, "NEW_LIFE_5": false}
var new_life : bool = false

#Set the global score, shots and timer for the HUD
var score: int = 0
var shots_left: int = 999
var timer: int = 999
var lives_left : int = MAX_LIVES_LEFT
var total_blobs : int = 0
var current_level : int = 1
var current_tank_health_level : float = 1.0 #starts at 1 and goes down
var camera_shake_intensity = 0.0
var camera_shake_duration = 0.0
var lose_tank_life : bool = false
var quit_current_game : bool = false

var screen_size = Vector2(1024, 640)

export var next_level = "res://scenes/levels/Level1.tscn"
var main_menu = "res://scenes/MainMenu.tscn"
var get_player_name = "res://scenes/GetPlayerName.tscn"

enum Type {Random, Sine, Noise}
var camera_shake_type = Type.Random

var noise : OpenSimplexNoise

func _ready():
	# Generate noise for noise shake
	#
	# This is only generated once when the game starts
	# and then read over and over again
	#
	# These parameters change the shape of the noise
	# and the feel of the shake6
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20
	noise.persistence = 0.8
	

func increment_score(increment_score) -> void:
	#We're called to increment the score by the nominated amount.
	score += increment_score
	#Let's check to see if the player should get a new life, but only once he's lost at least one life
	if lives_left < MAX_LIVES_LEFT:
		if score >= NEW_LIFE_5 and !new_life_achieved["NEW_LIFE_5"]: #We countdown from 5, to prevent a fizzbuzz issue. Also, we don't give the player this life again if they've already achieved.
			new_life_achieved["NEW_LIFE_5"] = true
			lives_left += 1
			new_life = true
		elif score >= NEW_LIFE_4 and !new_life_achieved["NEW_LIFE_4"]: 
			new_life_achieved["NEW_LIFE_4"] = true
			lives_left += 1
			new_life = true
		elif score >= NEW_LIFE_3 and !new_life_achieved["NEW_LIFE_3"]: 
			new_life_achieved["NEW_LIFE_3"] = true
			lives_left += 1
			new_life = true
		elif score >= NEW_LIFE_2 and !new_life_achieved["NEW_LIFE_2"]: 
			new_life_achieved["NEW_LIFE_2"] = true
			lives_left += 1
			new_life = true
		elif score >= NEW_LIFE_1 and !new_life_achieved["NEW_LIFE_1"]: 
			new_life_achieved["NEW_LIFE_1"] = true
			lives_left += 1
			new_life = true
	
	
func increment_total_blobs() -> void:
	#We're called to increment the b;obs by one
	total_blobs += 1
	
func deccrement_total_blobs() -> void:
	#We're called to decrement the b;obs by one
	total_blobs -= 1
	#We need to now check to see if the level is over.
	if total_blobs == 0:
		print("Level complete!")
	
func decrement_shots_left() -> void:
	#We're called to decrement the shots by one
	shots_left -= 1

func decrement_lives_left() -> void:
	#We're called to decrement the shots by one
	lives_left -= 1
	
func decrement_current_tank_health_level(hit_percentage) -> void:
	#We're called to decrement the health percentage of the current tank
	current_tank_health_level -= hit_percentage

func is_top_score() -> bool:
	#We call this function to see if the current score is high enough to be included in the top scores
	var sorted_scores = Array()

	#We have to find if the current score is bigger than any existing score. If so, add it 
	for i in range(top_scores.size()):
		print("Saving scores: " + str(top_scores.values()[i][0]) + ": " +  str(top_scores.values()[i][1]))
		sorted_scores.append([top_scores.values()[i][0], top_scores.values()[i][1]])
	
	#Sort the array, based on score
	sorted_scores.sort_custom(self, "custom_array_sort")
	
	#We need to re-write the top_scores
	top_scores.clear()
	#write the dictionary out as {0: ["conz", 1900], 1: ["eliana", 1950]]
	#We only want the top 50 scores. So, if more, we cap it:
	var top_score_num = sorted_scores.size() 
	if top_score_num > 50:
		 top_score_num = 50
	for i in range(top_score_num):
		top_scores[i] = [sorted_scores[i][0], sorted_scores[i][1]] 
		
	#Let's find the lowest of the top scores and see if it's below ours	
	if sorted_scores.size() > 0 and score > sorted_scores[top_score_num - 1][1]:
		return true
	elif  top_score_num < 50:
		#Even if we're not a high score, continue to add this score if less than 50 top scores
		return true
	else:
		return false
	
func load_scores():
	#print("LoadScores()")
	var file = File.new()
	if not file.file_exists(TOP_SCORES_FILE):
		print("No file saved!")
		return
	
	# Open existing file
	if file.open(TOP_SCORES_FILE, File.READ) != 0:
		print("Error opening file")
		return
	
	# Get the data
	var data = {}
	data = parse_json(file.get_line())
	
	var top_scores_text = "\t\t\t\t\tHall of Fame\n"
	if !data:
		return
	#var json = parse_json(file.get_line())
	# Then do what you want with the data
	for i in range(data.size()):
		top_scores[data.keys()[i]] = data.values()[i]
		top_scores_text += str("%04d" % int(data.keys()[i])) + ":\t " +  str("%010d" % data.values()[i][1]) + "\t\t" + str(data.values()[i][0]) + "\n"
	
	return top_scores_text
	
func save_current_score():
	#This is called when the the game is over so that the player's score can be added to the great list.
	print(str(top_scores))
	print("Adding user top score")
	top_scores[top_scores.size()] = [player_name, score] 
	print(str(top_scores))
	
func save_scores():
	#print("SaveScores()")
	# Open a file
	var file = File.new()
	if file.open(TOP_SCORES_FILE, File.WRITE) != 0:
		print("Error opening file")
		return
	
	var sorted_scores = Array()

	#We have to find if the current score is bigger than any existing score. If so, add it 
	for i in range(top_scores.size()):
		print("Saving scores: " + str(top_scores.values()[i][0]) + ": " +  str(top_scores.values()[i][1]))
		sorted_scores.append([top_scores.values()[i][0], top_scores.values()[i][1]])
	
	#print(str(sorted_scores))
#	print(str(sorted_names))
	#Sort the array, based on score
	sorted_scores.sort_custom(self, "custom_array_sort") #Don't know if this works!!
	#print(str(sorted_scores))
	
	#We need to re-write the top_scores
	top_scores.clear()
	#write the dictionary out as {0: ["conz", 1900], 1: ["eliana", 1950]]
	#We only want the top 50 scores. So, if more, we cap it:
	var top_score_num = sorted_scores.size() 
	if top_score_num > 50:
		 top_score_num = 50
	for i in range(top_score_num):
		top_scores[i] = [sorted_scores[i][0], sorted_scores[i][1]] 
	
	# Save the dictionary as JSON
	file.store_line(to_json(top_scores))
	file.close()
	
func custom_array_sort(a, b):
	# Check if the roll of a is less than the roll of b.
	return a[1] > b[1]


func shake(intensity, duration, type = Type.Random):
	# Set the shake parameters
	#
	# A good idea here is to add configuration settings that
	# allow the player to turn off shake
	#
	# if player_no_want:
	# 	intensity = 0
	
	if intensity > camera_shake_intensity and duration > camera_shake_duration:
		camera_shake_intensity = intensity
		camera_shake_duration = duration
		camera_shake_type = type


func _process(delta):
	# Get the camera
	#
	# You'll need to adjust this depending on how you want to
	# keep track of the active camera in your game
	#
	# Maybe your game has two cameras, maybe it has 10, who knows?
	# Do what you like
	if !is_instance_valid(get_tree()):
		#there is no parent node
		return
	elif !is_instance_valid(get_tree().current_scene):
		#there is no node
		return
	elif "Level" in get_tree().current_scene.name:
		#This is a Level, so shake
		var camera = get_tree().current_scene.get_node("ShakeCamera")
		if !camera:
			#there is no camera in this node
			return
			
		# Stop shaking if the camera_shake_duration timer is down to zero
		if camera_shake_duration <= 0:
			# Reset the camera when the shaking is done
			camera.offset = Vector2.ZERO
			camera_shake_intensity = 0.0
			camera_shake_duration = 0.0
			return
		
		# Subtract the elapsed time from the camera_shake_duration
		# so that it eventually ends
		#
		# You can do other fun stuff here too like have the intensity
		# decay gradually so that the shake tapers off
		camera_shake_duration = camera_shake_duration - delta
		
		# Shake it
		var offset = Vector2.ZERO
			
		if camera_shake_type == Type.Random:
			# Random shake
			# Chaos
			# Madness
			#
			# Personally, I like this best because players don't notice
			# any difference in the thick of battle when the shakes are short
			# and because it's dead simple.
			offset = Vector2(randf(), randf()) * camera_shake_intensity

		if camera_shake_type == Type.Sine:
			# Sine wave based shake
			#
			# Play around with the magic numbers to adjust the feel
			#
			# Basing the sine wave off of get_ticks_msec ensures that
			# the returned value is continuous and smooth
			offset = Vector2(sin(OS.get_ticks_msec() * 0.03), sin(OS.get_ticks_msec() * 0.07)) * camera_shake_intensity * 0.5
		
		if camera_shake_type == Type.Noise:
			# Noise based shake
			#
			# Accessing the noise based on get_ticks_msec ensures that
			# the returned value is continuous and smooth
			var noise_value_x = noise.get_noise_1d(OS.get_ticks_msec() * 0.1)
			var noise_value_y = noise.get_noise_1d(OS.get_ticks_msec() * 0.1 + 100.0)
			offset = Vector2(noise_value_x, noise_value_y) * camera_shake_intensity * 2.0
			
		camera.offset = offset
