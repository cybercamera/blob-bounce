extends CanvasLayer

var countdown_counter : int = 999
var bonus_total : int = 0

#var tank_lives = [$HBoxContainer/TankLive1, $HBoxContainer/TankLive2, $HBoxContainer/TankLive3, $HBoxContainer/TankLive4, $HBoxContainer/TankLive5]
var tank_lives = Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	$GameTimer.start()
	tank_lives = [$HBoxContainer/TankLive1, $HBoxContainer/TankLive2, $HBoxContainer/TankLive3, $HBoxContainer/TankLive4, $HBoxContainer/TankLive5]
	$InformationBox.visible = false
	#$InformationBox/Information.visible = false
	$InformationBox/Bonus.visible = false

func _process(delta):
	if Input.is_action_pressed("pause_toggle") and $PauseTimer.is_stopped(): 
		if Utils.paused: 
			#we're already paused, let's unpause
			hide_message()
			
		else:
			#let's pause
			show_message("Game Paused!")
			
			
		$PauseTimer.start() #we start the pause timer during which time the player cannot unpause, to prevent key-bounce
		Utils.paused = not Utils.paused
		get_tree().set_pause(Utils.paused)
		
	if Input.is_action_pressed("die"): 
		game_over()

func add_bonous_life():
	#We have to add a bonus life. We therefore add a full life, and make the rightmost tank modulated
	#print("add_bonous_life()")
	$BonusLife.play()
	show_title("Bonus Life!")
	tank_lives[Utils.lives_left - 2].modulate.a = 1.0
	tank_lives[Utils.lives_left - 1].modulate.a = Utils.current_tank_health_level

func game_over():
	#print("Game Over")
	Utils.lives_left = Utils.MAX_LIVES_LEFT
	if Utils.is_top_score(): #Are we a top score?
		get_tree().change_scene(Utils.get_player_name)
	else:
		get_tree().change_scene(Utils.main_menu)

func hide_message():
	$InformationBox.visible = false
	$InformationBox/Information.visible = false
	$InformationBox/Information.text = ""
	
func modulate_current_life():
	#The current tank has a health bar status. Depending on how good the health level
	#We are on this tank: Utils.lives_left
	#We need to set its modulation transparency levels
	#First, check to see if the modulation level has changed
	tank_lives[Utils.lives_left -1].modulate.a = Utils.current_tank_health_level
	#print("Utils.lives_left: " + str(Utils.lives_left) + " modulate.a: " + str(tank_lives[Utils.lives_left -1].modulate.a))

func show_info(info_text):
	$InformationBox/Information.text = info_text
	$InformationBox.visible = true
	$InformationBox/Information.visible = true
	
	
func show_bonus():
	#Calculate bonus. Bonus is current score + remaining time left on timer + remaining shots left
	#bonus_total = Utils.shots_left + int($Time.text)
	#Have changed the way that the bonus is computed. Now, higher time counters and higher cannonballs result in much higher bonus scores
	bonus_total = pow(Utils.shots_left/20.0, 2) + pow(int($Time.text)/20.0, 2)


func show_message(message_text):
	$InformationBox/Information.text = message_text
	$InformationBox.visible = true
	$InformationBox/Information.visible = true
	#$InformationBox/Information/InformationTimer.start()

func show_title(title_text):
	$InformationBox/Information.text = title_text
	$InformationBox.visible = true
	$InformationBox/Information.visible = true
	$InformationBox/Information/InformationTimer.start()


func _on_GameTimer_timeout():
	#print(str(tank_lives[1]))
	countdown_counter -= 1
	$Time.text = "Time: " + str(countdown_counter)
	$Score.text = "Score: " + str(Utils.score)
	$ShotsLeft.text = "Shots: " + str(Utils.shots_left)
	modulate_current_life()
	
	for i in range(Utils.lives_left, tank_lives.size()): #We need to hide all used tank lives
		tank_lives[i].visible = false
		
	if Utils.total_blobs == 0:
		$GameTimer.stop()
		$LevelOver.play()
		show_info("Level " + str(Utils.current_level) + " bonus")
		show_bonus()
		$NextLevelTimer.start()
			
	if Utils.lose_tank_life:
		Utils.decrement_lives_left()
		Utils.current_tank_health_level = 1.0 #reset the health level to 100% for the next tank
		Utils.lose_tank_life = false
		
	if Utils.lives_left <= 0:
		#Game over man, game over!
		#print("Game Oever")
		#game_over()
		#Play the game over music, which then triggers game_over()
		show_info("Game Over!")
		$GameOverMusic.play()
		$GameTimer.stop() #Stop the game timer so we don't get more actions.
		#print("Playing game over theme")
		
	if Utils.new_life:
		add_bonous_life()
		#Now that we've added the nonus life, make the semaphore mechanism false
		Utils.new_life = false
		
	if Utils.quit_current_game:
		Utils.quit_current_game = false
		get_tree().change_scene(Utils.main_menu)
		
func _on_InformationTimer_timeout():
	#Hide the information
	$InformationBox.visible = false
	$InformationBox/Information.visible = false
	$InformationBox/Bonus.visible = false

func _on_BonusTimer_timeout():
	var counter = int($InformationBox/Bonus.text) + 500
	$InformationBox/Bonus.text = str(counter)
	#print("bonus_total: " + str(bonus_total) + "counter: " + str(counter))
	if counter >= bonus_total:
		#We've reached our total bonus. Stop the timer.
		$InformationBox/Bonus/BonusTimer.stop()
		$InformationBox/Bonus.text = str(bonus_total)
		Utils.score = Utils.score + bonus_total #Add our totak to the existing score
		$Score.text = "Score: " + str(Utils.score) #Update the existing score
		$InformationBox/Information/InformationTimer.start() #Start the time to make this information visible for a few seconds
		


func _on_LevelOver_finished():
	#The level over sound has finished playing. Let's do the counter
	$InformationBox/Bonus/BonusTimer.start()
	$CounterClicks.play()
	$InformationBox/Bonus.visible = true
	

func _on_NextLevelTimer_timeout():
	get_tree().change_scene(Utils.next_level)
	queue_free()



func _on_GameOverMusic_finished():
	#print("_on_GameOverMusic_finished()")
	game_over()
