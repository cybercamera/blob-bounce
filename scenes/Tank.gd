extends KinematicBody2D

signal health_changed
signal dead

var cannonball_scene = preload("res://scenes/Cannonball.tscn")

#export (PackedScene) var Cannonball
export (int) var speed = 200
export (float) var rotation_speed = 1
export (float) var gun_cooldown = 0.4
export (int) var health = 1000

var velocity = Vector2()
var can_shoot = true
var alive : bool = true
var time_start = 0

var tank_shift_distance_start = Vector2()
var tank_shift_distance_end = Vector2()
var tank_currently_hit = false

func _ready():
	$GunTimer.wait_time = gun_cooldown
	add_to_group("tank")

func control(delta):
	$Turret.look_at(get_global_mouse_position())
	var rot_dir = 0
	if Input.is_action_pressed("turn_right"):
		rot_dir += 1
	if Input.is_action_pressed("turn_left"):
		rot_dir -= 1
	rotation += (rotation_speed * rot_dir * delta)
	velocity = Vector2()

	if Input.is_action_pressed("forward"):
		velocity = Vector2(speed, 0 ).rotated(rotation)
		#Blow out some smoke
		EmitSmoke(4)
		
		
	if Input.is_action_pressed("backward"):
		velocity = Vector2(-speed/2, 0).rotated(rotation)
		#Blow out some smoke
		EmitSmoke(2)
		
	if (velocity.length() * speed) > 0:
		if !$TankSound.playing:
			$TankSound.play()

	if Input.is_action_pressed("fire"):
		if can_shoot:
			shoot_cannonball()
			
	if Input.is_action_just_pressed("ui_cancel"):
		Utils.quit_current_game = true
		
#	if Input.is_action_pressed("pause_toggle"):
#		#Player selected to pause the game
#		Utils.paused = not Utils.paused
		
	
func _physics_process(delta):
	if not alive:
		return
	control(delta)
	move_and_slide(velocity)
	if tank_currently_hit:
		tank_shift_distance_end = position

func EmitSmoke(smoke_duration:int) -> void:
	#first check to see if there's a parent
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a smoke particle and attach it to the parent level 
		var smoke_scene = load("res://scenes/Smoke.tscn")
		var smoke_puff = smoke_scene.instance()
		get_parent().add_child(smoke_puff) #Add to level node
		smoke_puff.position = $ExhaustPoint.global_position
		smoke_puff.lifetime = smoke_duration
		smoke_puff.emitting = true

func shoot_cannonball():
	can_shoot = false
	#time_counter()
	$GunTimer.start()
	if !$FireSound.playing:
		$FireSound.play()
	var new_cannonball = cannonball_scene.instance()
	get_parent().add_child(new_cannonball)
	new_cannonball.position = $Turret/Muzzle.global_position
	#print("new_cannonball.position: " + str(new_cannonball.position))
	var fire_dir = Vector2()
	fire_dir = get_global_mouse_position() - $Turret/Muzzle.global_position
	fire_dir = fire_dir.normalized()
	#print("$Turret.rotation: " + str($Turret.rotation))
	new_cannonball.apply_impulse(Vector2(), fire_dir * new_cannonball.speed)
	$Turret/Muzzle/MuzzleFlash.visible = true
	$MuzzleFlashTimer.start()
	Utils.decrement_shots_left()
	

func time_counter():
	var time_now = OS.get_unix_time()
	var elapsed = time_now - time_start
	time_start = time_now
	var minutes = elapsed / 60
	var seconds = elapsed % 60
	var str_elapsed = "%02d : %02d" % [minutes, seconds]
	print("elapsed : ", str(time_now) + " : " + str_elapsed)

func _on_GunTimer_timeout():
	can_shoot = true

func calculate_hit(explosion_pos):
	#Calculate hit strangth, which is the distance of the
	var explosion_distance = (position - explosion_pos).length()
	#print("Explosion distance: " + str(explosion_distance))
	if explosion_distance > 0:
		var health_hit : float = (300.0 - explosion_distance) #The larger the distance, the weaker the hit
		health -= health_hit
		Utils.current_tank_health_level = health/1000.0 #compute the 0 to 1 percentage
		#print("Health Hit: " + str(health_hit))
		#print("health: " + str(health))
		if health <= 100.0: #We need a lower threshold below which a tank is no longer viable/visible
			print("Utils.lives_left: " + str(Utils.lives_left))
			if Utils.lives_left > 1:
				Utils.current_tank_health_level = 0.0
				health = 1000 #if the health level is low enough, reset this next tank's health
				Utils.lose_tank_life = true
			else:
				#Last tank lost. Hide it.
				#print("Hide tank")
				Utils.lose_tank_life = true
				dissolve_tank()
				
func dissolve_tank():
	#last tank is lost. Let's tween it down to no visibility.
	$DissolveTween.interpolate_property(self, "modulate", Color(self.modulate.r, self.modulate.g, self.modulate.b, self.modulate.a) , Color(self.modulate.r, self.modulate.g, self.modulate.b, 0), 2.4, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$DissolveTween.start()

func _on_MuzzleFlashTimer_timeout():
	$Turret/Muzzle/MuzzleFlash.visible = false


func _on_Area2D_body_entered(body):
	if body.get_groups().has("cannonball") or body.get_groups().has("block"):
		if body.exploded == true:
			#print("Tank takes a hit!")
			if !tank_currently_hit:
				tank_currently_hit = true
				tank_shift_distance_start = position
				calculate_hit(body.position)
				


func _on_Area2D_body_exited(body):
	if tank_currently_hit:
		#Tank is currently being bit by a cannonball, which has now stopped
		tank_currently_hit = false
		#calculate_hit()


func _on_DissolveTween_tween_completed(object, key):
	self.visible = false
	queue_free()
