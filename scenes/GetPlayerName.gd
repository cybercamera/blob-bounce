extends TextureRect


# Called when the node enters the scene tree for the first time.
func _ready():
	$PlayerName.text = Utils.player_name
	

func _on_OK_pressed():
	Utils.player_name = $PlayerName.text
	Utils.save_current_score()
	Utils.save_scores()
	get_tree().change_scene(Utils.main_menu)
